function create(type, duration, location) {
    if (PhotonDrop.Storage.queryAll("session", {
            limit: 1,
            sort: [
                ["created_time", "DESC"]
            ],
            query: (row) => {
                return (row.status != 2) ? true : false;
            }
        }).length > 0) {
        PhotonDrop.Site.Alert.toast('error', 'Error creating the session', 'There\'s still an open session. Please end it first.');
        return;
    }

    PhotonDrop.Storage.insert("session", {
        type,
        duration,
        location,
        status: 0,
        created_time: new Date().valueOf(),
        started_time: null,
        ended_time: null
    });
    PhotonDrop.Storage.commit();
    PhotonDrop.Site.Alert.toast('success', `New session created`, `Type: ${type} - Duration: ${duration} minutes - Location: ${location}`);
    console.log({
        type,
        duration,
        location,
        status: 0,
        created_time: new Date().valueOf(),
        started_time: null
    });
}

function start() {
    let getSession = PhotonDrop.Storage.queryAll("session", {
        limit: 1,
        sort: [
            ["created_time", "DESC"]
        ],
        query: (row) => {
            return (row.status == 0) ? true : false;
        }
    });
    if (getSession.length > 0) {
        PhotonDrop.Storage.update("session", {
            ID: getSession[0].ID
        }, (row) => {
            row.status = 1;
            row.started_time = new Date().valueOf();
            return row;
        });
        PhotonDrop.Storage.commit();
        PhotonDrop.Site.Alert.toast('success', 'Session started', `Session was started just now!`);
        return;
    } else {
        PhotonDrop.Site.Alert.toast('error', 'Error creating the session', 'There\'s no unstarted session right now.');
        return;
    }
}

function extend() {
    let getSession = PhotonDrop.Storage.queryAll("session", {
        limit: 1,
        sort: [
            ["created_time", "DESC"]
        ],
        query: (row) => {
            return (row.status != 2) ? true : false;
        }
    });
    if (getSession.length > 0) {
        PhotonDrop.Site.smalltalk.prompt('Extend Session Time', 'Please enter the duration in minutes, the session should be extended by, below:', '10')
            .then((result) => {
                result = parseInt(result);
                PhotonDrop.Storage.update("session", {
                    ID: getSession[0].ID
                }, (row) => {
                    row.duration = parseInt(row.duration) + parseInt(result);
                    return row;
                });
                if (PhotonDrop.Storage.commit()) {
                    PhotonDrop.Site.Alert.toast('success', 'Session extended', `Session was extended by ${result}. New Duration is ${parseInt(getSession[0].duration) + parseInt(result)}.`);
                } else {
                    PhotonDrop.Site.Alert.toast('error', 'Error extending the session', 'Something went wrong. Please try again.');
                }
            });
    } else {
        PhotonDrop.Site.Alert.toast('error', 'Error extending the session', 'There\'s no session open right now.');
        return;
    }
}

function end() {
    PhotonDrop.Site.smalltalk.confirm("Are you sure?", "This action will end ALL currently open drops and set them to 'ended'").then(() => {
        PhotonDrop.Storage.update("session", (row) => {
            return (row.status !== 2) ? true : false;
        }, (row) => {
            row.status = 2;
            row.started_time = (row.started_time) ? row.started_time : new Date().valueOf();
            row.ended_time = new Date().valueOf();
            return row;
        });
        if (PhotonDrop.Storage.commit()) {
            PhotonDrop.Site.Alert.toast('success', 'Session ended', `All open sessions have been ended!`);
        } else {
            PhotonDrop.Site.Alert.toast('error', 'Error ending all sessions', 'Something weird happened.');
        }
    });
}

function status() {
    let getSession = PhotonDrop.Storage.queryAll("session", {
        limit: 1,
        sort: [
            ["created_time", "DESC"]
        ],
        query: (row) => {
            return (row.status != 2) ? true : false;
        }
    });
    if (getSession.length > 0) {
        return getSession[0];
    } else {
        return false;
    }
}

function addUserToDrop(sessionid, userid) {
    if (!status()) return false;
    sessionid = parseInt(sessionid);
    userid = userid;
    if (PhotonDrop.Storage.queryAll("session_dropees", {
            query: (row) => {
                return (row.sessionid == sessionid && row.userid == userid) ? true : false;
            },
            limit: 1
        }).length == 0) {
        PhotonDrop.Storage.insert("session_dropees", {
            sessionid,
            userid
        });
    }
    PhotonDrop.Storage.commit();
}

function removeUserFromDrop(userid) {
    let session = PhotonDrop.Session.status();
    userid = userid;
    PhotonDrop.Storage.deleteRows('session_dropees', {
        sessionid: session.ID,
        userid
    });
    PhotonDrop.Site.Alert.toast('success', 'User removed', 'The specified user has been removed from the drop.');
}

function parseDropees(dropBotMsg) {
    let m, regex = /Discord: (.*) - ID: (\d*) - SC: (.*) - Drops Attended: (\d*)/g,
        session = PhotonDrop.Session.status();
    if (session) {
        while ((m = regex.exec(dropBotMsg)) !== null) {
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }

            if (PhotonDrop.UserManager.add(m[2], m[1], m[3], parseInt(m[4]) + 1)) {
                PhotonDrop.Site.Alert.toast("success", "Successfully added user", `${m[1]}, Drops: ${parseInt(m[4])+1}`, false);
            }
            PhotonDrop.Session.addUserToDrop(session.ID, m[2]);
        }
    }
}

function parseMessage(msg, userid = null) {
    msg = msg.replace(/%started%/g, moment(status().started_time).fromNow(true)); // started
    msg = msg.replace(/%duration%/g, status().duration);
    msg = msg.replace(/%remaining%/g, moment(status().started_time).add(status().duration, 'm').fromNow(true));
    msg = msg.replace(/%location%/g, status().location);
    if (userid !== null) {
        msg = `<@${userid}>\n` + msg;
        msg = msg.replace(/%warns_curr%/g, PhotonDrop.UserManager.get(userid).warns.length);
        msg = msg.replace(/%warns_max%/g, PhotonDrop.Site.Settings().threshold.warns);
    }
    if (PhotonDrop.Site.Settings().announcements_with_mention && userid === null) {
        let dropees = PhotonDrop.Storage.queryAll("session_dropees", {
            query: {
                sessionid: status().ID
            }
        });
        let mentions = [];
        dropees.forEach((dropee) => mentions.push(`<@!${dropee.userid}>`));
        msg = mentions.join(' ') + '\n' + msg;
    }
    return msg;
}

function writeEnhancedDiscord() {
    if (PhotonDrop.Site.Settings().discord.enhanceddiscord) {
        const fs = require('fs');
        let stylesheet = fs.readFileSync(PhotonDrop.Site.Settings().discord.enhanceddiscord);
        stylesheet = stylesheet.toString().split("\n");
        let startLine = stylesheet.indexOf('/* PHOTONDROP START */');
        let endLine = stylesheet.indexOf('/* PHOTONDROP END */');
        stylesheet.splice(startLine, (endLine - startLine) + 1);

        stylesheet.push('/* PHOTONDROP START */');
        if (PhotonDrop.Session.status()) {
            let dropees = PhotonDrop.Storage.queryAll("session_dropees", {
                query: {
                    sessionid: status().ID
                }
            });

            dropees.forEach((dropee) => {
                stylesheet.push('.avatar-large[style*="' + dropee.userid + '"]+.comment .username-wrapper:after,');
                stylesheet.push('.avatar-small[style*="' + dropee.userid + '"]+.member-inner .member-username-inner:after,');
            });
            stylesheet.push('.photondrop-dropees {');
            stylesheet.push('    content: \'Dropee\';');
            stylesheet.push('    background: rgba(255, 255, 255, 0.3);');
            stylesheet.push('    color: #fff;');
            stylesheet.push('    margin-left: 5px;');
            stylesheet.push('    padding: 2px 5px;');
            stylesheet.push('    border-radius: 5px;');
            stylesheet.push('    font-size: 11px;');
            stylesheet.push('    text-transform: uppercase;');
            stylesheet.push('    vertical-align: bottom;');
            stylesheet.push('}');
        }
        stylesheet.push('/* PHOTONDROP END */');

        fs.writeFileSync(PhotonDrop.Site.Settings().discord.enhanceddiscord, stylesheet.join('\n'));
    }
}

module.exports = {
    create,
    start,
    status,
    extend,
    end,
    parseDropees,
    addUserToDrop,
    removeUserFromDrop,
    parseMessage,
    writeEnhancedDiscord
};