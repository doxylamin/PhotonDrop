$(() => {
    $.ajax({
        url: "https://www.rockstargames.com/rockstarsupport2a/status.json",
        method: "GET",
        json: true
    }).done((data) => {
        data.services.forEach((element) => {
            $("table.rsg-status > thead > tr").append(`<th>${element.name}</th>`);
            switch (element.service_status.status) {
                case 'UP':
                    $("table.rsg-status > tbody > tr.status").append(
                        `<td class="success"><i class="ion-chevron-up"></i> <b>${element.service_status.status}</b></td>`
                    );
                    break;
                case 'LIMITED':
                    $("table.rsg-status > tbody > tr.status").append(
                        `<td class="warning"><i class="ion-chevron-right"></i> <b>${element.service_status.status}</b></td>`
                    );
                    break;
                case 'DOWN':
                    $("table.rsg-status > tbody > tr.status").append(
                        `<td class="danger"><i class="ion-chevron-down"></i> <b>${element.service_status.status}</b></td>`
                    );
                    break;
                default:
                    $("table.rsg-status > tbody > tr.status").append(
                        `<td class="info"><i class="ion-ios-help-empty"></i> <b>${element.service_status.status}</b></td>`
                    );
                    break;
            }

            $("table.rsg-status > tbody > tr.updated").append(
                `<td>${new Date(element.updated).toLocaleString()}</td>`);
        });
    }).fail(() => console.log('failed'));

    // region dropHistory
    let dropHist = PhotonDrop.Storage.queryAll("session", { sort: [["id", "desc"]] });
    let dropHistTable = $("table.drop-history").DataTable();
    dropHist.forEach((drop) => {
        dropHistTable.row.add([
            drop.ID,
            drop.type,
            drop.duration + ' minutes',
            drop.location,
            new Date(drop.created_time).toLocaleString(),
            (drop.started_time)?new Date(drop.started_time).toLocaleString():'not started yet',
            (drop.ended_time)?new Date(drop.ended_time).toLocaleString():'still running',
            moment(drop.ended_time).diff(drop.started_time, 'minutes') + ' minutes'
        ]).draw();
    });

    const {remote} = require('electron');
    if(!remote.app.getVersion().includes("tester")) {
        $("div.alert").hide();
    }
});