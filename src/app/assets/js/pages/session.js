var dropeeTable = $("#dropeeTable").DataTable(),
    timeReached = false;

$(() => {
    $('#dropeeTable tbody').on('click', 'tr', function () {
        var data = dropeeTable.row(this).data();
        $("#dropee-action").modal('show', data[0]);
    });

    $('#dropee-action').on('show.bs.modal', function (event) {
        modalPutInfo($(this), PhotonDrop.UserManager.get(event.relatedTarget));
    });

    $('#dropee-action').on('hide.bs.modal', function (event) {
        var modal = $(this);
        modal.find('.modal-body').find("[class^='dbl-']").html("");
        modal.find('.modal-body').find("[class^='scl-']").html("");
    });

    $('div#session-create').on('show.bs.modal', function (event) {
        if (localStorage.getItem('session_defaults')) {
            let sessdef = JSON.parse(localStorage.getItem('session_defaults'));
            $(this).find('select#cs-type').find(`option[value="${sessdef.type}"]`).prop('selected', true);
            $(this).find('input#cs-duration').val(sessdef.duration);
            $(this).find('input#cs-location').val(sessdef.location);
        }
    });

    var sessionCreationForm = $('div#session-create').find('form');
    sessionCreationForm.on('submit', (event) => {
        event.preventDefault();
        PhotonDrop.Session.create(sessionCreationForm.find("select#cs-type").val(), sessionCreationForm.find("input#cs-duration").val(), sessionCreationForm.find("input#cs-location").val());
        $('div#session-create').modal('hide');
        populateDropeeTable();
        $('div#session-parse').modal('show');
    });

    var sessionDropeeForm = $('div#session-parse').find('form');
    sessionDropeeForm.on('submit', (event) => {
        event.preventDefault();
        PhotonDrop.Session.parseDropees(sessionDropeeForm.find("textarea#ps-dropees").val());
        $('div#session-parse').modal('hide');
        populateDropeeTable();
    });

    let sessionAnnForm = $("div#session-announcement").find("form");
    $('div#session-announcement').on('show.bs.modal', function (event) {
        sessionAnnForm.find("select#sa-chooser").empty();
        sessionAnnForm.find("select#sa-chooser").append(`<option value="empty">Empty Text</option>`);
        let anns = PhotonDrop.Texts.getTextsByScope('announcement');
        anns.forEach((ann) => {
            sessionAnnForm.find("select#sa-chooser").append(`<option value="${ann.ID}">${ann.title}</option>`);
        });
    });

    sessionAnnForm.find("select#sa-chooser").on('change', () => {
        let id = sessionAnnForm.find("select#sa-chooser").val();
        if (id === "empty") {
            sessionAnnForm.find("textarea#sa-text").val("");
        } else {
            let text = PhotonDrop.Texts.getTextById(parseInt(id));
            sessionAnnForm.find("textarea#sa-text").val(text.content);
        }
    });

    sessionAnnForm.on('submit', (e) => {
        e.preventDefault();
        PhotonDrop.Common.Clipboard.writeText(PhotonDrop.Session.parseMessage(sessionAnnForm.find("textarea#sa-text").val()));
        $("div#session-announcement").modal('hide');
        PhotonDrop.Site.Alert.toast('success', 'Text copied', 'Text was successfully copied to clipboard');
    });

    let sessionWarnForm = $("div#session-warning").find("form");
    $('div#session-warning').on('show.bs.modal', function (event) {
        sessionWarnForm.find("input#warn-receiver").val(event.relatedTarget);
        sessionWarnForm.find("select#warn-chooser").empty();
        sessionWarnForm.find("select#warn-chooser").append(`<option value="empty">Empty Text</option>`);
        let reasons = PhotonDrop.Texts.getTextsByScope('warning');
        reasons.forEach((reason) => {
            sessionWarnForm.find("select#warn-chooser").append(`<option value="${reason.ID}">${reason.title}</option>`);
        });
    });

    sessionWarnForm.find("select#warn-chooser").on('change', () => {
        let id = sessionWarnForm.find("select#warn-chooser").val();
        if (id === "empty") {
            sessionWarnForm.find("textarea#warn-reason").val("");
        } else {
            let text = PhotonDrop.Texts.getTextById(parseInt(id));
            sessionWarnForm.find("textarea#warn-reason").data('name', text.title);
            sessionWarnForm.find("textarea#warn-reason").val(text.content);
        }
    });

    sessionWarnForm.on('submit', (e) => {
        e.preventDefault();
        let warnReason = sessionWarnForm.find("textarea#warn-reason");
        let warnReceiver = sessionWarnForm.find("input#warn-receiver");
        if (PhotonDrop.UserManager.warn(PhotonDrop.Session.status().ID, warnReceiver.val(), warnReason.data('name'))) {
            PhotonDrop.Common.Clipboard.writeText(
                PhotonDrop.Session.parseMessage(
                    warnReason.val(),
                    warnReceiver.val()
                )
            );
            PhotonDrop.Site.Alert.toast("warning", "User has been warned", warnReason.data('name'));
        }
        $("div#session-warning").modal('hide');
        populateDropeeTable();
    });

    setInterval(updateSessionInfo, 500);

    populateDropeeTable();
});

function updateSessionInfo() {
    let currentSession = PhotonDrop.Session.status();
    if (currentSession) {
        $("div.session-info").find("td.sess-id").text(currentSession.ID);
        $("div.session-info").find("td.sess-type").text(currentSession.type);
        $("div.session-info").find("td.sess-started").text((currentSession.started_time !== null) ? countdown(currentSession.started_time).toString() + ' ago' : 'not started yet');
        $("div.session-info").find("td.sess-duration").text(currentSession.duration + ' minutes');
        $("div.session-info").find("td.sess-location").text(currentSession.location);
        $("div.session-info").find("td.sess-remaining").text(countdown(moment(currentSession.started_time || new Date().valueOf()).add(currentSession.duration, 'm')).toString());
        if (currentSession.started_time !== null) {
            if (moment().isAfter(moment(currentSession.started_time).add(currentSession.duration, 'm'))) {
                if (timeReached === false) {
                    PhotonDrop.Site.Alert.toast('info', 'Planned time reached', 'You\'ve reached your planned droptime.', false);
                    PhotonDrop.Site.Alert.sound('alerts_melodic_02', true);
                    timeReached = true;
                }
                $("div.session-info").find("td.sess-remaining").html('<b>Drop ended ' + countdown(moment(currentSession.started_time || new Date().valueOf()).add(currentSession.duration, 'm')).toString() + ' ago</b>');
            } else {
                $("div.session-info").find("td.sess-remaining").text(countdown(moment(currentSession.started_time || new Date().valueOf()).add(currentSession.duration, 'm')).toString());
            }
        }
    } else {
        $("div.session-info").find("[class^='sess-']").text("-/-");
    }
}

function modalPutInfo(modal, user) {
    modal.find(".modal-footer").find("button").unbind();
    //region dblookup
    modal.find(".modal-body").find("td.dbl-userid").html(user.userid);
    modal.find(".modal-body").find("td.dbl-username").html(user.username);
    modal.find(".modal-body").find("td.dbl-socialclub").html(user.socialclub);
    modal.find(".modal-body").find("td.dbl-attended-global").html(user.attended);
    modal.find(".modal-body").find("td.dbl-attended-mine").html(user.attended_internal.length);
    modal.find(".modal-body").find("td.dbl-warnings").html(user.warns.length);
    modal.find(".modal-body").find("td.dbl-blacklist").html((user.blacklist) ? 'Yes' : 'No');
    modal.find(".modal-body").find("td.dbl-notes").html((user.notes.length > 0) ? user.notes : 'empty');

    modal.find(".modal-footer").find("button.btn-warning").on('click', () => {
        $(modal).modal('hide');
        $("#session-warning").modal('show', user.userid);
    });
    modal.find(".modal-footer").find("button.btn-danger").on('click', () => {
        $(modal).modal('hide');
        PhotonDrop.Session.removeUserFromDrop(user.userid);
        populateDropeeTable();
    });
    //endregion
    //region sclookup
    $.ajax({
            url: "https://socialclub.rockstargames.com/games/gtav/career/overviewAjax?nickname=" + user.socialclub + "&slot=Freemode",
            method: 'GET',
            headers: {
                'Accept-Language': 'en',
                'Content-Language': 'en'
            }
        })
        .done(function (data) {
            if ($(data).prop('id') == 'sectionBlockedStats' || !data.includes("window.SCSettings.nickname = '" + user.socialclub.toLowerCase() + "';") || data.includes("window.SCSettings.nickname = '';")) {
                PhotonDrop.Site.Alert.toast('error', 'Could not retrieve Social Club Data', 'No stats available for ' + user.socialclub + ' due to privacy settings');
                modal.find('.modal-body').find("[class^='scl-']").html("- N/A -");
                return;
            }

            var scData = {
                "rank": parseInt($(data).find("#freemodeRank > div.rankHex > h3").text()),
                "rankRP": $(data).find("#freemodeRank > div.rankXP > div.clearfix > h3").text(),
                "rankBar": $(data).find("#freemodeRank > div.rankXP > div.rankBar > h4").text().split(':')[1],
                "money": {
                    "cash": parseInt($(data).find("#cash-value").text().replace(/[^\d]/g, '')),
                    "bank": parseInt($(data).find("#bank-value").text().replace(/[^\d]/g, '')),
                },
                "crew": {
                    "name": $(data).find(".crewCard").find('h3 > a').text(),
                    "tag": $(data).find(".crewCard").find('div.crewTag > span').text(),
                    "avatar": $(data).find(".crewCard").find('img.avatar').attr('src'),
                }
            };

            modal.find(".modal-body").find("td.scl-username").html(user.socialclub);
            modal.find(".modal-body").find("td.scl-rank").html(scData.rank);
            modal.find(".modal-body").find("td.scl-rank-rp").html(scData.rankRP);
            modal.find(".modal-body").find("td.scl-play-time").html(scData.rankBar);
            modal.find(".modal-body").find("td.scl-money-cash").html(Number(scData.money.cash).toLocaleString());
            modal.find(".modal-body").find("td.scl-money-bank").html(Number(scData.money.bank).toLocaleString());
            if (Number(scData.money.bank + scData.money.cash) >= Number(PhotonDrop.Site.Settings().threshold.money)) {
                modal.find(".modal-body").find("td.scl-money-total").html('<span class="label label-danger">' + Number(scData.money.bank + scData.money.cash).toLocaleString() + '</span>');
            } else {
                modal.find(".modal-body").find("td.scl-money-total").html(Number(scData.money.bank + scData.money.cash).toLocaleString());
            }
            modal.find(".modal-body").find("img.scl-crew-img").attr('src', scData.crew.avatar);
            modal.find(".modal-body").find("span.scl-crew-name").html(scData.crew.name);
            modal.find(".modal-body").find("span.scl-crew-tag").html(`<span class="badge badge-primary">${scData.crew.tag}</span>`);
        })
        .fail(function (xhr) {
            modal.find('.modal-body').find("[class^='scl-']").html("- N/A -");
            console.log(xhr);
            PhotonDrop.Site.Alert.toast('error', 'Social Club Lookup', 'Server returned error: ' + xhr.status);
        });
    //endregion
}

function populateDropeeTable() {
    dropeeTable.clear();

    let session = PhotonDrop.Session.status();
    if (session) {
        let dropees = PhotonDrop.Storage.queryAll("session_dropees", {
            query: {
                sessionid: session.ID
            }
        });

        dropees.forEach((dropee) => {
            dropee = PhotonDrop.UserManager.get(dropee.userid);
            let sessionWarns = 0;
            dropee.warns.forEach((warn) => {
                if (warn.sessionid == session.ID) sessionWarns++;
            });

            let warnElement = "",
                maxWarns = PhotonDrop.Site.Settings().threshold.warns;
            if (sessionWarns > maxWarns) {
                warnElement = `<span class="badge badge-danger">Session: ${sessionWarns}</span><span class="badge badge-warning">Total: ${dropee.warns.length}</span>`;
            } else {
                warnElement = `<span class="badge badge-warning">Session: ${sessionWarns}</span><span class="badge badge-warning">Total: ${dropee.warns.length}</span>`;
            }

            let newUser = dropeeTable.row.add([
                dropee.userid,
                dropee.socialclub,
                dropee.username,
                `<span class="badge badge-info">Total: ${dropee.attended}</span><span class="badge badge-info">Mine: ${dropee.attended_internal.length}</span>`,
                warnElement,
            ]);
            newUser.node().id = dropee.userid;
        });

    }
    dropeeTable.draw(false);
    PhotonDrop.Session.writeEnhancedDiscord();
}

function extendSession() {
    PhotonDrop.Session.extend();
    timeReached = false;
};

function copyDropeeTags() {
    if(!PhotonDrop.Session.status()) return;
    let dropees = PhotonDrop.Storage.queryAll("session_dropees", {
        query: {
            sessionid: PhotonDrop.Session.status().ID
        }
    });
    let mentions = [];
    dropees.forEach((dropee) => mentions.push(`<@${dropee.userid}>`));
    PhotonDrop.Common.Clipboard.writeText(mentions.join(' '));
    PhotonDrop.Site.Alert.toast('success', 'Dropee Tags copied to clipboard', '');
}